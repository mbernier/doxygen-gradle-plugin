//
// ============================================================================
// (C) Copyright Schalk W. Cronje 2013-2021
//
// This software is licensed under the Apache License 2.0
// See http://www.apache.org/licenses/LICENSE-2.0 for license details
//
// Unless required by applicable law or agreed to in writing, software distributed under the License is
// distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and limitations under the License.
//
// ============================================================================
//

package org.ysb33r.gradle.doxygen

import org.gradle.testkit.runner.GradleRunner
import org.ysb33r.gradle.doxygen.impl.DoxygenDistributionDownloader
import org.ysb33r.gradle.doxygen.testfixtures.DownloadTestSpecification
import spock.lang.IgnoreIf
import spock.lang.TempDir

import java.nio.file.Files

/**
 * @author Schalk W. Cronjé
 */
@IgnoreIf({ DownloadTestSpecification.SKIP_TESTS || !DoxygenDistributionDownloader.DOWNLOAD_SUPPORTED })
class DoxygenExecutableSpec extends DownloadTestSpecification {

    public static final File TESTFSREADROOT = new File(System.getProperty('TESTFSREADROOT') ?: 'src/downloadTest/resources')
    public static final File TESTFSWRITEROOT = new File(System.getProperty('TESTFSWRITEROOT') ?: 'build/tmp/downloadTest', 'DoxygenExecutableSpec')
    public static final File DOXY_TEMPLATE = new File(System.getProperty('DOXY_TEMPLATE') ?: 'src/main/resources/doxyfile-template.dox')

    @TempDir
    File testProjectDir
    File buildFile
    File projectDir
    File buildDir
    File settingsFile
    File testkitDir
    File sourceDir

    void setup() {
        projectDir = new File(testProjectDir, 'test-project')
        testkitDir = new File(testProjectDir, 'testkit')
        buildFile = new File(projectDir, 'build.gradle')
        buildDir = new File(projectDir, 'build')
        settingsFile = new File(projectDir, 'settings.gradle')
        sourceDir = new File(projectDir, 'src/main/cpp')
        projectDir.mkdirs()
        testkitDir.mkdirs()

        settingsFile.text = ''

        buildFile.text = """
        plugins {
            id 'org.ysb33r.doxygen'
        }
        """.stripIndent()

        sourceDir.mkdirs()
        Files.copy(
            new File(TESTFSREADROOT, 'sample-cpp/sample.cpp').toPath(),
            new File(sourceDir, 'sample.cpp').toPath()
        )
    }

    void "Run Doxygen to generate simple documentation with a default template"() {
        setup:
        buildFile << """
        doxygen {
            outputDir 'build/docs'
            
            options generate_xml: false,
                generate_latex: false,
                generate_html: true,
                have_dot: false,
                aliases: 'ChangeLog=\\\\xrefitem ChangeLogs "ChangeLog" "ChangeLogs" '
       
        }
        """.stripIndent()

        when:
        final result = gradleRunner.build()

        then:
        new File(buildDir, 'docs/html').exists()
        new File(buildDir, 'docs/html/index.html').exists()
    }


    void "When 'template' is supplied as a string, configuration should still work"() {
        setup: 'A task configured with a custom template which is supplied as a string'
        buildFile << """
        doxygen {
            outputDir 'build/docs'
            
            options generate_xml: false,
                generate_latex: false,
                generate_html: true,
                have_dot: false,
                aliases: 'ChangeLog=\\\\xrefitem ChangeLogs "ChangeLog" "ChangeLogs" '
       
            template '${DOXY_TEMPLATE.absolutePath.replace('\\', '\\\\')}'
        }
        """.stripIndent()

        when: 'The task is executed'
        final result = gradleRunner.build()
        def lines = new File(buildDir, 'tmp/doxygen.doxyfile').text.readLines()

        then: 'The HTML files should have been created'
        new File(buildDir, 'docs/html').exists()
        new File(buildDir, 'docs/html/index.html').exists()

        and: 'Lines from the custom template should have been used'
        lines.find { 'FILE_PATTERNS =' }
    }

    private GradleRunner getGradleRunner() {
        GradleRunner.create()
            .withProjectDir(projectDir)
            .withTestKitDir(testkitDir)
            .withArguments('doxygen', '-i', '-s', "-Porg.ysb33r.gradle.doxygen.download.url=${DOWNLOAD_CACHE_DIR.toURI()}")
            .withPluginClasspath()
            .forwardOutput()
            .withDebug(true)
    }

}