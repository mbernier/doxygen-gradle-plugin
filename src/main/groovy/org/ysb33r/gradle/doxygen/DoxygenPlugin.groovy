//
// ============================================================================
// (C) Copyright Schalk W. Cronje 2013-2021
//
// This software is licensed under the Apache License 2.0
// See http://www.apache.org/licenses/LICENSE-2.0 for license details
//
// Unless required by applicable law or agreed to in writing, software distributed under the License is
// distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and limitations under the License.
//
// ============================================================================
//

package org.ysb33r.gradle.doxygen

import groovy.transform.CompileStatic
import org.gradle.api.Plugin
import org.gradle.api.Project
import org.ysb33r.grolifant.api.core.ProjectOperations

/**
 * Created by schalkc on 29/05/2014.
 */
@CompileStatic
class DoxygenPlugin implements Plugin<Project> {
    public static final String DOX_TASK_NAME = 'doxygen'
    public static final String DOX_TEMPLATE_TASK_NAME = 'createDoxygenTemplates'
    public static final String DOX_GROUP = 'Documentation'
    void apply(Project project) {

        project.apply(plugin: 'base')
        final po = ProjectOperations.maybeCreateExtension(project)
        final tasks = po.tasks

        tasks.register(DOX_TASK_NAME, Doxygen) { t ->
            t.identity {
                group = DOX_GROUP
                source 'src/main/cpp',
                    'src/main/headers',
                    'src/main/asm',
                    'src/main/objectiveC',
                    'src/main/objectiveCpp',
                    'src/main/c'
            }
        }

        tasks.register(
            DOX_TEMPLATE_TASK_NAME,
            DoxygenTemplateFiles,
            [tasks.taskProviderFrom(DOX_TASK_NAME)] as List<Object>
        )

        tasks.named(DOX_TEMPLATE_TASK_NAME) { t ->
            t.identity {
                group = DOX_GROUP
            }
        }
    }
}