//
// ============================================================================
// (C) Copyright Schalk W. Cronje 2013-2021
//
// This software is licensed under the Apache License 2.0
// See http://www.apache.org/licenses/LICENSE-2.0 for license details
//
// Unless required by applicable law or agreed to in writing, software distributed under the License is
// distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and limitations under the License.
//
// ============================================================================
//

package org.ysb33r.gradle.doxygen.impl

import groovy.transform.CompileStatic
import org.gradle.api.provider.Provider
import org.gradle.util.GradleVersion
import org.ysb33r.grolifant.api.core.OperatingSystem
import org.ysb33r.grolifant.api.core.ProjectOperations
import org.ysb33r.grolifant.api.errors.DistributionFailedException

/**
 * Downloads specific versions of Doxygen.
 * Currently limited to Linux, Windows & MacOS X on x86 32 + 64-bit architectures as these are the only ones for which
 * binary packages are available from the Doxygen site.
 */
@CompileStatic
class DoxygenDistributionDownloader extends BaseDistributionInstaller {
    public static final OperatingSystem OS = OperatingSystem.current()
    public static final boolean DOWNLOAD_SUPPORTED = OS.linux || OS.macOsX || OS.windows

    DoxygenDistributionDownloader(final ProjectOperations projectOperations) {
        super(
            'doxygen',
            'native-binaries/doxygen',
            'https://downloads.sourceforge.net/project/doxygen',
            'doxygen',
            projectOperations
        )
    }

    /** Provides an appropriate URI to download a specific verson of Doxygen.
     *
     * @param ver Version of Doxygen to download
     * @return URI for Linux, Windows (32,64 bit) or MacOSX. {@code null} otherwise
     */
    @Override
    URI uriFromVersion(final String ver) {
        final String base = "${baseURI.get()}/rel-${ver}"
        if (OS.isWindows()) {
            // Using GradleVersion as it has a handy version comparison
            if (GradleVersion.version(ver) >= GradleVersion.version('1.8.0') && System.getProperty('os.arch').contains('64')) {
                "${base}/doxygen-${ver}.windows.x64.bin.zip".toURI()
            } else {
                "${base}/doxygen-${ver}.windows.bin.zip".toURI()
            }
        } else if (OS.isLinux()) {
            "${base}/doxygen-${ver}.linux.bin.tar.gz".toURI()
        } else if (OS.isMacOsX()) {
            "${base}/Doxygen-${ver}.dmg".toURI()
        } else {
            null
        }
    }

    @Override
    File getByVersion(String version) {
        getDoxygenExecutablePath(version).get()
    }

    /** Adds a special verification case for Doxygen Windows binaries.
     *
     * The Windows distribution is not zipped into a subfolder and needs extra care.
     * For Linux & MacOsX it will use the default implementation.
     *
     * @param distDir Directory where distribution was unpacked to.
     * @return Distribution directory
     */
    @Override
    protected File verifyDistributionRoot(File distDir) {
        if (OS.windows) {
            if (!new File(distDir, 'doxygen.exe').exists()) {
                throw new DistributionFailedException("Doxygen does not contain 'doxygen.exe'.")
            }
            return distDir
        } else {
            super.verifyDistributionRoot(distDir)
        }
    }

    /** Returns the path to the Doxygen executable.
     * Will force a download if not already downloaded.
     *
     * @return Location of {@code doxygen} or null if not a supported operating system.
     */
    Provider<File> getDoxygenExecutablePath(String version) {
        if (OS.isWindows()) {
            getDistributionFile(version, 'doxygen.exe')
        } else if (OS.isLinux()) {
            getDistributionFile(version, 'bin/doxygen')
        } else if (OS.isMacOsX()) {
            getDistributionFile(version, 'Contents/Resources/doxygen')
        } else {
            null
        }
    }

    /** Unpacks a Doxygen archive.
     * For {@code zip} and {@code tar.gz} formats the standard operations will be used.
     * For {@code dmg} formats will call out to {@code hdiutil} to temporary mount/unmount image.
     *
     * @param srcArchive Location of downloaded archive.
     * @param destDir Directory to unpack to.
     */
    @Override
    protected void unpack(final File srcArchive, final File destDir) {
        if (OS.macOsX && srcArchive.name.endsWith('.dmg')) {
            unpackDMG(srcArchive, destDir)
        } else {
            super.unpack(srcArchive, destDir)
        }
    }
}

